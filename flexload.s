; Loads and run flex.sys image file from DriveWire server
; Uses transfer record to determine end of file
;
; S.Orchard Nov 2021


    org $4000

    orcc #$50

    ldx <$88      ; get cursor pos from BASIC
    stx textpos

    ldx #loadmsg  ; loading message
    jsr outstrz

    sta $ffdf     ; map1

    lds #$c080

    ldx #buffer+256
    stx bufptr

    ; command to mount the flex image
    ldx #loadcmd
    ldy #loadcmdsize
    jsr DWWrite

    ; drivewire returns a drive number to use
    ldx #drivenum
    ldy #1
    jsr DWRead
    bne dwerror
    tst drivenum
    beq dwerror

    ; process the records within the flex image
1   bsr read_byte
    tsta
    beq 1b        ; null record
    cmpa #2
    beq 2f        ; binary record

    cmpa #$16     ; transfer record
    bne recerror
    bsr read_address
    jmp [address]

2   lda #'. | $40 ; indicate progress
    jsr outchar
    bsr read_address
    bsr read_byte ; record length
    sta count
    beq 1b        ; zero length record
    ldx address
3   bsr read_byte
    sta ,x+
    dec count
    bne 3b
    bra 1b

; show error message then halt
dwerror
    ldx #dwerrmsg
    bra 1f
recerror
    ldx #recerrmsg
1   jsr outstrz
9   bra 9b

read_address
    bsr read_byte
    sta address
    bsr read_byte
    sta address+1
    rts

read_byte
    pshs x
    ldx bufptr
    cmpx #buffer+256
    blo 1f
    bsr get_sector
    bne dwerror
    inc lsnlo
    ldx #buffer
1   lda ,x+
    stx bufptr
9   puls x,pc

get_sector
    ldx #readcmd
    ldy #5
    jsr DWWrite
    ldy #256
    bsr DWRead
    bne 9f

    ldx #temp
    sty ,x            ; checksum
    ldy #2
    jsr DWWrite

    ldx #temp         ; status
    ldy #1
    jsr DWRead
    bne 9f
    ldb temp
9   rts


 IF BECKER
    include "dwbecker.s"
 ELSE
    include "dwserial.s"
    setdp -1    ; dwserial uses dp
 ENDIF

outstrz
1   lda ,x+
    beq 2f
    jsr outchar
    bra 1b
2   rts

outchar
    ldu textpos
    cmpa #13
    bne 1f
    tfr u,d
    andb #$e0
    tfr d,u
    leau 32,u
    bra 2f
1   sta ,u+
2   stu textpos
9   rts


loadmsg   fcv "LOADING FLEX",0
dwerrmsg  fcv "\rDRIVEWIRE ERROR",0
recerrmsg fcv "\rFLEX RECORD ERROR",0

loadcmd   fcc 1,8,"flex.sys"
loadcmdsize equ *-loadcmd

readcmd   fcb 0xd2
drivenum  rmb 1
lsnhi     fcb 0,0
lsnlo     fcb 0
buffer    rmb 256
temp      rmb 2

bufptr    rmb 2
address   rmb 2
count     rmb 1

textpos   rmb 2
