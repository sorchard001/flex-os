ASM=asm6809 -v
BIN2CMD=tools/ddbin2flex.py
CFG=config.s
DRVDEPS=$(CFG) drivers.s diskio.s console.s con_vectors.s
DRVDEPS+= keyboard.s dwbecker.s dwserial.s con32.s con51.s cs.dat

.PHONY: all
all: flex.sys FLEXLOAD.BIN disks

drivers.bin con_vectors.bin &: $(DRVDEPS)
	$(ASM) -D -o drivers.bin -E console.inc -l drivers.lst $(CFG) drivers.s
	$(ASM) -D -o con_vectors.bin -l con_vectors.lst $(CFG) con_vectors.s

flex.sys: flex.cor drivers.bin con_vectors.bin
	$(BIN2CMD) drivers.bin drv.sys --notransfer
	$(BIN2CMD) con_vectors.bin convec.sys --transfer 0xcd00
	cat flex.cor drv.sys convec.sys > $@

FLEXLOAD.BIN: flexload.s $(CFG)
	$(ASM) -D -l flexload.lst -o $@ $(CFG) $<

.PHONY: disks
disks:
	$(MAKE) -C disks

.PHONY: clean
clean:
	$(MAKE) -C disks clean
	rm -f *.bin *.BIN
	rm -f *.inc
	rm -f *.sys
	rm -f *.lst
