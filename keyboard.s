; Dragon 32/64 keyboard scan routine
; by Ciaran Anscomb, 2007

pia0_ddra	equ	$ff00
pia0_pdra	equ	$ff00
pia0_cra	equ	$ff01
pia0_ddrb	equ	$ff02
pia0_pdrb	equ	$ff02
pia0_crb	equ	$ff03
pia1_ddra	equ	$ff20
pia1_pdra	equ	$ff20
pia1_cra	equ	$ff21
pia1_ddrb	equ	$ff22
pia1_pdrb	equ	$ff22
pia1_crb	equ	$ff23


		section "code"

kbd_init
		pshs	d,x
		;lda #$20
		clra
		sta	kbd_capslock
		ldx	#kbd_rollover
		ldb	#8
1		sta	,x+
		decb
		bne	1B
		puls	d,x,pc


; rather long but accurate keyboard scanning routine
kbd_scan	pshs	b,x
		; check for any newly pressed keys
		clr	tmp0
		ldx	#kbd_rollover
		ldb	#$fe
10		stb	tmp1
		stb	pia0_pdrb
		lda	pia0_pdra
		ora	#$80
		coma
		tfr	a,b
		ora	,x
		eora	,x
		stb	,x+
		beq	70F
		ldb	#$f8
1		addb	#8
		lsra
		bcs	60F
		bne	1B
70		inc	tmp0
		ldb	tmp1
		lslb
		orb	#$01
		bcs	10B
		bra	98F
		; have a scancode - find its position in the matrix
60		addb	tmp0
		ldx	#kbd_matrix
		abx
		; check shift state (shift, clear)
		ldd	#$7f40
		sta	pia0_pdrb
		bitb	pia0_pdra	; shift pressed?
		bne	1F
		leax	56,x
		bra	2F
1		lda	#$fd
		sta	pia0_pdrb
		bitb	pia0_pdra	; clear pressed?
		bne	2F
		leax	112,x
		; abort if any joystick firebutton is pressed
2		lda	#$ff
		sta	pia0_pdrb
		lda	pia0_pdra
		ora	#$80
		coma
		bne	98F
		; fetch translated value
		lda	,x
		bmi	kbd_set_state
1		cmpa	#'a
		blo	99F
		cmpa	#'z
		bhi	99F
		eora	kbd_capslock
99		tsta
		puls	b,x,pc
kbd_set_state	cmpa	#$ff
		beq	kbd_toggle_capslock
		bra	98F
kbd_toggle_capslock
		ldb	kbd_capslock
		eorb	#$20
		stb	kbd_capslock
98		clra
		puls	b,x,pc

; *** Keyboard matrix ***
; Maps scancodes + shift value to ascii values.
; Special values: $ff = toggle capslock

kbd_matrix
		; key
		fcb	 '0,  '1,  '2,  '3,  '4,  '5,  '6,  '7
		fcb	 '8,  '9,  ':, $3b, $2c,  '-,  '.,  '/
		fcb	 '@,  'a,  'b,  'c,  'd,  'e,  'f,  'g
		fcb	 'h,  'i,  'j,  'k,  'l,  'm,  'n,  'o
		fcb	 'p,  'q,  'r,  's,  't,  'u,  'v,  'w
		fcb	 'x,  'y,  'z,  '^, $0a, $08, $09, $20
		fcb	$0d,   0, $03,   0,   0,   0,   0,   0
		; shift + key
		fcb	$ff,  '!,  '",  '#,  '$,  '%,  '&,  ''
		fcb	 '(,  '),  '*,  '+,  '<,  '=,  '>,  '?
		fcb	$13,  'A,  'B,  'C,  'D,  'E,  'F,  'G
		fcb	 'H,  'I,  'J,  'K,  'L,  'M,  'N,  'O
		fcb	 'P,  'Q,  'R,  'S,  'T,  'U,  'V,  'W
		fcb	 'X,  'Y,  'Z,  '_,  '[, $7f,  '], $20
		fcb	$0d, $0c, $03,   0,   0,   0,   0,   0
		; clear + key
		fcb	  0,  '|,   0, $1b, $1c, $1d, $1e,  '`
		fcb	 '{,  '},   0,   0,   0,  '~,   0,  '\
		fcb	$11, $01, $02, $03, $04, $05, $06, $07
		fcb	$08, $09, $0a, $0b, $0c, $0d, $0e, $0f
		fcb	$10, $11, $12, $13, $14, $15, $16, $17
		fcb	$18, $19, $1a,   0,   0, $02, $06,   0
		fcb	$0d,   0,   0,   0,   0,   0,   0,   0
kbd_end


		section "convars"

; variables

kbd_capslock	rmb	1

; general-purpose temporary
tmp0		rmb	1
tmp1		rmb	1

kbd_rollover	rmb	8
