; Dragon drivers for FLEX
;
; S.Orchard October 2021


; disk i/o jump table
; memory after table is available for driver code
;
; console vector table is handled separately as it
; is not contiguous with rest of driver code

        section "code"

        org $de00

READ    jmp dk_read       ; read sector
WRITE   jmp dk_write      ; write sector
VERIFY  jmp dk_verify     ; verify last sector written
RESTORE jmp dk_restore    ; restore head to track #0
DRIVE   jmp dk_select     ; select drive
CHKRDY  jmp dk_check      ; check drive ready
QUICK   jmp dk_qcheck     ; quick check drive ready
INIT    jmp dk_init       ; cold start init
WARM    jmp dk_warm       ; warm start init
SEEK    jmp dk_seek       ; seek track


; Flex reserves d370 - d3e0 for user code/vars
        section "convars"
        org $d370

; other variables placed after driver code
        section "vars"
        org end_code

        include "diskio.s"
        include "console.s"


; get end of each section

        section "convars"
end_convars equ *       ; this must be below d3e1

        section "code"
end_code  equ *

        section "vars"
end_vars  equ *         ; this must be below the video base address
