
CON_VIDEND    equ $fdff
CON_VIDBASE    equ CON_VIDEND - $17ff


        section "code"

; wait for key press with flashing cursor
; returns key in A
con_waitkey
        pshs b
        ldd #1
        std con_ccount
1       ldd con_ccount
        subd #1
        bne 2f
        lda #127
        jsr con_putchar
        ldd #750
2       std con_ccount
        jsr  kbd_scan
        beq  1b
        puls b
        jmp con_putblank


; wait for key and echo to screen
; returns key in A

con_echokey
        bsr con_waitkey
con_outch
        pshs d
        anda #$7f
        cmpa #$20
        bhs 5f

        cmpa #13
        beq con_cr
        cmpa #08
        beq con_bs
        cmpa #09
        beq con_tab
        cmpa #10
        beq con_lf
        cmpa #12
        bne 9f
        jmp con_ff

5       jsr con_putchar
        ldd con_textposx
        addd #$00a0
7       std con_textposx
        subd #$1fe0
        blo 9f
        clra
        clrb
        std con_textposx
con_lf
        lda con_textposy
        cmpa #23
        bhs con_scroll
        inc con_textposy
9       puls d,pc

con_cr
        clra
        clrb
        std con_textposx
        puls d,pc

con_bs
        ldd con_textposx
        subd #$a0
        bpl 1f
        lda con_textposy
        beq 2f
        dec con_textposy
        ldd #$1f40
1       std con_textposx
        jsr con_putblank
2       puls d,pc


; exploits a quirk in the numbers:
; tab stops are the only positions with zero pixel shift
con_tab
        ldd con_textposx
        andb #$e0         ; ensure loop terminates
1       addd #$a0         ; advance one character...
        tstb              ;
        bne 1b            ; ...until we have zero pix shift
        bra 7b            ; check if new line required


con_scroll
        pshs x,u
        clra
        clrb
        std con_textposx
        ldx #CON_VIDBASE
        lda #23
2       ldb #128
1       ldu 256,x
        stu ,x++
        decb
        bne 1b
        deca
        bne 2b

        ldd #$ffff
1       std ,x++
        cmpx #CON_VIDEND
        blo 1b

        puls x,u
        puls d,pc


con_ff
        pshs x,u
        clra
        clrb
        std con_textpos
        sta con_textpix
        ldx #CON_VIDBASE
        ldu #$ffff
1       stu ,x++
        cmpx #CON_VIDEND
        blo 1b
        puls x,u
        puls d,pc


; draw character at current position
; character code in A

con_putchar

        pshs d,u,x
        suba #32          ; char code
        lsra              ; even/odd into carry
        bcs 1f            ; odd

        ldb #$f0          ; even char is rhs of source byte
        ldx #con_shifttab0
        bra 2f

1       ldb #$0f          ; odd char is rhs of source byte
        ldx #con_shifttab1

2       stb 11f+1         ; source mask
        ldb #7
        mul
        addd #con_charset
        tfr d,u           ; source address

        lda con_textpix
        lsra
        lsra
        lsra
        lsra
        ldd a,x
        sta 10f+1         ; pixel shift
        sex               ; address offset
        addd con_textpos
        adda #CON_VIDBASE/256
        tfr d,x           ; destination address

        lda #7
        sta con_temp
10      lda #0            ; pixel shift (self-mod)
        ldb ,u+
11      andb #$00         ; source mask (self-mod)
        mul
        eora ,x
        eorb 1,x
        std ,x
        leax 32,x
        dec con_temp
        bne 10b
        puls d,u,x,pc

; multipliers used to shift character into position
; 1st number is shift, 2nd is address offset
con_shifttab1
        fcb 8,-1,4,-1,2,-1,1,-1
con_shifttab0
        fcb 128,0,64,0,32,0,16,0,8,0,4,0,2,0,1,0


; erase character at current position
; used by cursor and backspace

con_putblank

        pshs d,x

        lda con_textpix
        lsra
        lsra
        lsra
        lsra
        ldx #con_blanktab
        ldd a,x
        std 10f+1

        ldx con_textpos
        leax CON_VIDBASE,x

        lda #7
        sta con_temp
10      ldd #0
        ora ,x
        orb 1,x
        std ,x
        leax 32,x
        dec con_temp
        bne 10b
        puls d,x,pc


; character erase data for each degree of shift
CON_ERASE    equ $f0
con_blanktab
        fdb CON_ERASE*$80,CON_ERASE*$40,CON_ERASE*$20,CON_ERASE*$10
        fdb CON_ERASE*$08,CON_ERASE*$04,CON_ERASE*$02,CON_ERASE*$01


; console driver specific setup

con_init2
        lda #$f0
        sta $ff22
        sta $ffc5
        sta $ffc3
        rts

con_charset
        includebin "cs.dat"


        section "convars"

con_textpos
con_textposy   rmb 1
con_textposx   rmb 1
con_textpix    rmb 1
con_ccount     rmb 2
con_temp       rmb 1
