#!/usr/bin/python3

"""
Tool for building Flex disk images or extracting files
from existing images

Specify -h for help on options

S.Orchard Nov 2021
"""

import datetime
import argparse
import os
import itertools

# threshold for y2k correction
PIVOTYEAR = 1975


class FlexDiskError(Exception):
    def __init__(self, msg=None, addinfo=None):
        if msg is None:
            msg = 'Error processing Flex disk image'
        if not addinfo is None:
            msg = '{} ({})'.format(msg, addinfo)
        super().__init__(msg)

class FlexIllegalFilenameError(FlexDiskError):
    def __init__(self, fname='File', *args):
        super().__init__(
            '{} is not a legal filename for Flex'.format(fname), *args)

class FlexFileExistsError(FlexDiskError):
    def __init__(self, fname='File', *args):
        super().__init__(
            '{} already exists in Flex disk directory'.format(fname), *args)

class FlexFileNotFoundError(FlexDiskError):
    def __init__(self, fname='File', *args):
        super().__init__(
            '{} not found in Flex disk directory'.format(fname), *args)

class FlexDiskFullError(FlexDiskError):
    def __init__(self, fname='file', *args):
        super().__init__(
            'Unable to write {} - Flex disk is full'.format(fname), *args)

class FlexDiskZeroLenError(FlexDiskError):
    def __init__(self, fname='file', *args):
        super().__init__('Unable to write {} '
            '- Flex does not allow zero length files'.format(fname), *args)

class FlexEndChainError(FlexDiskError):
    def __init__(self, *args):
        super().__init__(
            'Attempt to read past end of sector chain', *args)

class FlexRandomMapFullError(FlexDiskError):
    def __init__(self, fname='file', *args):
        super().__init__(
            'Unable to write {} - random map is full'.format(fname), *args)


# check if string is ok to use for Flex filename
# name and extension should be checked separately
def flexlegalname(fname):
    if len(fname) == 0:
        return False
    if not fname[0].isalpha():
        return False
    for c in fname[1:]:
        if not (c.isalnum() or (c in ['-', '_'])):
            return False
    return True


# Property helper functions
# These provide a means of accessing various data types
#   at specific offsets in a byte array
# Saves a lot of repetition

def byteproperty(index):
    i = index
    def get(self):
        return self.bytes[i]
    def set(self, val):
        self.bytes[i] = val
    return property(fget=get, fset=set)

def wordproperty(index):
    i = index
    def get(self):
        return (self.bytes[i] << 8) + self.bytes[i+1]
    def set(self, val):
        self.bytes[i] = (val >> 8)
        self.bytes[i+1] = val & 0xff
    return property(fget=get, fset=set)

def linkproperty(index):
    i = index
    def get(self):
        return self.bytes[i], self.bytes[i+1]
    def set(self, link):
        self.bytes[i] = link[0]
        self.bytes[i+1] = link[1]
    return property(fget=get, fset=set)

def dateproperty(index):
    i = index
    def get(self):
        mm = self.bytes[i]
        dd = self.bytes[i+1]
        yy = self.bytes[i+2] + PIVOTYEAR - (PIVOTYEAR % 100)
        if yy < PIVOTYEAR:
            yy += 100
        try:
            d = datetime.date(yy, mm, dd)
        except:
            d = self.bytes[i+2], self.bytes[i+1], self.bytes[i]
        return d
    def set(self, date):
        self.bytes[i] = date.month
        self.bytes[i+1] = date.day
        self.bytes[i+2] = date.year % 100
    return property(fget=get, fset=set)

def textproperty(index, length):
    i = index
    j = i + length
    def get(self):
        return ''.join(chr(b) for b in self.bytes[i:j] if b != 0)
    def set(self, text):
        self.bytes[i:j] = bytearray(length)
        for k, c in enumerate(text[:length]):
            self.bytes[i+k] = ord(c) & 0xff
    return property(fget=get, fset=set)


# generic sector
class Sector:
    """class representing a generic Flex sector

    fwdlink
    recnum
    data
    """
    fwdlink = linkproperty(0)
    recnum = wordproperty(2)

    def __init__(self):
        self.bytes = bytearray(256)

    @property
    def data(self):
        """get/set the 252 byte payload of the sector"""
        return memoryview(self.bytes)[4:256]

    @data.setter
    def data(self, data):
        dlen = len(data) + 4
        self.bytes[4:dlen] = data
        if dlen < 256:
            self.bytes[dlen:256] = bytearray(256-dlen)


# system information record
class SIRsector(Sector):
    """class representing Flex system information record sector

    volname
    volnum
    datastart
    dataend
    numdatasectors
    date
    maxtrack
    maxsector
    """

    volname = textproperty(16, 11)
    volnum = wordproperty(27)
    datastart = linkproperty(29)
    dataend = linkproperty(31)
    numdatasectors = wordproperty(33)
    date = dateproperty(35)
    maxtrack = byteproperty(38)
    maxsector = byteproperty(39)

    def __init__(self, num_tracks, sectors_per_track):
        super().__init__()

        volname = 'VOLUME'
        volnum = 1
        date = datetime.date.today()

        self.volname = volname
        self.volnum = volnum
        self.datastart = 1, 1
        self.dataend = num_tracks-1, sectors_per_track
        self.numdatasectors = (num_tracks-1)*sectors_per_track
        self.date = date
        self.maxtrack = num_tracks-1
        self.maxsector = sectors_per_track


class DirEntry:
    """class representing a Flex directory entry

    name
    ext
    attr
    start
    end
    size
    hasmap
    date
    isempty
    fname
    __init__
    __str__
    """
    name = textproperty(0, 8)
    ext = textproperty(8, 3)
    attr = byteproperty(11)
    # reserved 12
    start = linkproperty(13)
    end = linkproperty(15)
    size = wordproperty(17)
    hasmap = byteproperty(19)
    # reserved 20
    date = dateproperty(21)

    @property
    def isempty(self):
        """returns True if directory entry available"""
        return (len(self.name) == 0) or (ord(self.name[0]) > 127)

    @property
    def fname(self):
        """get/set filename as string in form 'name.ext'"""
        return '{}.{}'.format(self.name, self.ext)

    @fname.setter
    def fname(self, fname):
        s = fname.split('.')
        if (len(s) != 2
                or not flexlegalname(s[0])
                or not flexlegalname(s[1])):
            raise FlexIllegalFilenameError(fname)
        self.name = s[0]
        self.ext = s[1]

    def __init__(self, bytes):
        self.bytes = bytes

    def __str__(self):
        """friendly string version of directory entry

        intended for printing directory listing
        """
        def attr(ch, bit):
            return ('-', ch)[(self.attr >> bit) & 1]
        astr = '{}{}{}{}'.format(
            attr('W', 7), attr('D', 6), attr('R', 5), attr('C', 4))
        return '{:8s}.{:3s} {:5d}  {}  {}'.format(
            self.name, self.ext, self.size, self.date, astr)


class FlexDisk:
    """class representing Flex disk image
    """
    def _initdirectoryrecords(self):
        """Associate directory records with disk sectors"""
        self.direntries = []
        for sector in range(4, self.sir.maxsector):
            view = memoryview(self.sectors[sector].bytes)
            for rec in range(10):
                offset = 16 + rec*24
                self.direntries.append(DirEntry(view[offset:offset+24]))


    # create a blank disk image
    def format(self, numtracks, numsectors):
        """Create a blank disk image

        numtracks  : number of tracks
        numsectors : number of sectors per track
        """
        self.sectors = []
        sec = None
        for track in range(numtracks):
            for sector in range(1, numsectors+1):
                if sec is not None:
                    sec.fwdlink = track, sector
                sec = Sector()
                self.sectors.append(sec)

        # add system info record
        self.sir = SIRsector(numtracks, numsectors)

        # associate directory records with disk sectors
        self._initdirectoryrecords()

        # terminate directory chain at end of track 0
        self.sectors[numsectors-1].fwdlink = 0, 0


    def loadfromfile(self, fname):
        """load disk image from file"""
        self.sectors = []
        with open(fname, 'rb') as f:
            while True:
                sec = Sector()
                sec.bytes = bytearray(f.read(256))
                if len(sec.bytes) < 256:
                    break
                self.sectors.append(sec)

        self.sir.__class__ = SIRsector

        # associate directory records with disk sectors
        self._initdirectoryrecords()


    def savetofile(self, fname):
        """save disk image to file"""
        with open(fname, 'wb') as f:
            for s in self.sectors:
                f.write(s.bytes)


    @property
    def sir(self):
        """get/set system information record sector"""
        return self.sectors[2]

    @sir.setter
    def sir(self, sir):
        self.sectors[2] = sir


    def lsn(self, link):
        """return the logical sector number specified by the link"""
        return link[0]*self.sir.maxsector + link[1] - 1


    def getsector(self, link):
        """return the sector specified by the link"""
        lsn = self.lsn(link)
        if lsn >= 0:
            return self.sectors[lsn]
        else:
            return None


    #def nextlink(self, link):
    #    """return next link in sector chain"""
    #    s = self.getsector(link)
    #    if s is None:
    #        raise FlexEndChainError()
    #    else:
    #        return s.fwdlink


    def getdirentry(self, fname, raiseerror=True):
        """Get directory entry matching given filename

        args:
            fname      : filename to find
            raiseerror : True to raise FlexFileNotFoundError

        returns:
            DirEntry instance or None if not found
        """
        entry = None
        for d in self.direntries:
            if d.fname == fname:
                entry = d
                break;
        if raiseerror and (entry is None):
            raise FlexFileNotFoundError(fname)
        return entry


    @property
    def getdir(self):
        """yields all non-empty directory entries

        yields DirEntry instances
        """
        for d in self.direntries:
            if not d.isempty:
                yield d


    def getfilesectors(self, file):
        """yields all sectors in a file

        file is specified either by name or as DirEntry
        """
        if not (type(file) is DirEntry):
            file = self.getdirentry(file)
        link = file.start
        while link != (0, 0):
            s = self.getsector(link)
            link = s.fwdlink
            yield s


    def getfiledata(self, file):
        """yields all data in a file

        file is specified either by name or as DirEntry

        yields single byte values
        """
        for s in self.getfilesectors(file):
            for b in s.data:
                yield b


    def addfile(self, fname, fdata, random=False, handlezerolen=False):
        """Add file to Flex disk image

        args:
            fname  : name of file to add to Flex image
            fdata  : file as iterable sequence of byte values
            random : set True to add random access map
        """
        def getsector(link):
            s = self.getsector(link)
            if s is None:
                raise FlexDiskFullError(fname, 'Not enough free sectors')
            return s

        if self.getdirentry(fname, False):
            raise FlexFileExistsError(fname)

        found = False
        for d in self.direntries:
            if d.isempty:
                found = True
                break;

        if not found:
            raise FlexDiskFullError(fname, 'No directory entries available')

        startlink = self.sir.datastart
        curlink = startlink
        filelen = 0

        if random:
            # allocate two sectors for random map
            mapsectors = []
            for i in range(2):
                s = getsector(curlink)
                s.recnum = 0
                s.data = bytearray(252)
                curlink = s.fwdlink
                mapsectors.append(s)

        map = []
        endlink = curlink
        while len(fdata) > 0:
            s = getsector(curlink)
            if self.lsn(curlink) - self.lsn(endlink) == 1:
                map[-1][1] += 1
            else:
                map.append([curlink, 1])
            filelen += 1
            s.recnum = filelen
            s.data = fdata[:252]
            fdata = fdata[252:]
            endlink = curlink
            curlink = s.fwdlink

        if filelen == 0:
            if handlezerolen:
                return
            else:
                raise FlexDiskZeroLenError(fname)

        if random:
            filelen += 2
            # write random map to map sectors
            mptr = 0
            msec = 0
            for link, size in map:
                mapsectors[msec].data[mptr:mptr+3] = bytes(
                    [link[0], link[1], size])
                mptr += 3
                if mptr >= 252:
                    mptr = 0
                    msec += 1
                    if msec >= 2:
                        raise FlexRandomMapFullError(fname)

        s.fwdlink = 0, 0    # last sector has eof mark
        d.fname = fname
        d.size = filelen
        d.start = startlink
        d.end = endlink
        d.date = datetime.date.today()
        d.hasmap = (0, 2)[random]
        self.sir.datastart = curlink
        self.sir.numdatasectors -= filelen


    def __str__(self):
        """friendly string disk info"""
        sir = self.sir
        return ('Volume {} #{}  {}\n'
                '{} tracks {} sectors/track\n'
                '{} free sectors').format(
            sir.volname,
            sir.volnum,
            sir.date,
            sir.maxtrack+1,
            sir.maxsector,
            sir.numdatasectors)


if __name__ == '__main__':

    def check_intrange(value, min, max, description):
        try:
            i=int(value)
        except ValueError:
            i = min-1
        if i < min or i > max:
            raise argparse.ArgumentTypeError(
                '{} must be in range {}-{}'.format(description, min, max))
        return i

    def check_numtracks(value):
        return check_intrange(value, 2, 255, 'Number of tracks')

    def check_numsectors(value):
        return check_intrange(value, 5, 255, 'Number of sectors')


    parser = argparse.ArgumentParser(
            description='TSC Flex disk image tool  (S.Orchard Nov 2021)')

    #grp = parser.add_mutually_exclusive_group()

    parser.add_argument('dskfile', help='filename for Flex disk image')
    parser.add_argument('-n', '--newdisk', action='store_true',
        help='Create an empty disk image')
    parser.add_argument('-t', '--tracks', type=check_numtracks, default=40,
        help='Specify number of tracks for new disk')
    parser.add_argument('-s', '--sectors', type=check_numsectors, default=18,
        help='Specify number of sectors per track for new disk')
    parser.add_argument('-x', '--extract', dest='flexfile', nargs='+',
        help='Extract files from existing Flex image and save in '
                'current directory. Specify "all" to extract all files.')
    parser.add_argument('-c', '--compression', action='store_true',
        help='Handle space compression in extracted TXT files')
    parser.add_argument('-a', '--addfile', nargs='+',
        help='Add files to Flex disk image')
    parser.add_argument('-r', '--random', action='store_true',
        help='Create random access maps when adding files')
    parser.add_argument('-z', '--zerolencheck', action='store_true',
        help='Silently reject zero length files instead of stopping with error')
    parser.add_argument('-d', '--dir', action='store_true',
        help='List Flex disk directory to standard output (after other operations)')

    args = parser.parse_args()

    dsk = FlexDisk()

    if args.newdisk:
        dsk.format(args.tracks, args.sectors)
        modified = True
    else:
        dsk.loadfromfile(args.dskfile)
        modified = False

    if args.flexfile:
        if args.flexfile[0] == 'all':
            files = [d.fname for d in dsk.getdir]
        else:
            files = args.flexfile
        for fname in files:
            d = dsk.getdirentry(fname)
            data = dsk.getfiledata(d)
            if d.hasmap:    # remove map from random file
                data = itertools.islice(data, 504, None)
            if args.compression and fname[-4:] == '.TXT':
                opdat = bytearray()
                tabflag = False
                for b in data:
                    if tabflag:
                        opdat.extend(b' ' * b)
                        tabflag = False
                    elif b == 9:
                        tabflag = True
                    else:
                        opdat.append(b)
                data = opdat
            with open(fname, 'wb') as f:
                f.write(bytes(data))

    if args.addfile:
        for fname in args.addfile:
            flexname = os.path.basename(fname)
            with open(fname, 'rb') as f:
                dsk.addfile(flexname, f.read(),
                    random=args.random,
                    handlezerolen=args.zerolencheck)
            modified = True

    if args.dir:
        print('Contents of {}:'.format(args.dskfile))
        print(dsk)
        for d in dsk.getdir:
            print(d)

    if modified:
        dsk.savetofile(args.dskfile)
