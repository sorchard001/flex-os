#!/usr/bin/python3

"""
Converts a DragonDOS binary into a FLEX binary
Optionally adds a transfer record

Specify -h for help on options

S.Orchard Nov 2021
"""

import sys
import struct
import argparse

if __name__ == '__main__':

    def check_xfer(value):
        try:
            i=int(value, 0)
        except ValueError:
            i = min-1
        if i < 0 or i > 0xffff:
            raise argparse.ArgumentTypeError(
                'Transfer address must be in range 0x0000 - 0xffff')
        return i


    parser = argparse.ArgumentParser(
        description='DragonDOS to TSC Flex binary conversion tool  (S.Orchard Nov 2021)')

    parser.add_argument('ddosfilename', help='DragonDOS binary input file')
    parser.add_argument('flexfilename', help='Flex command output file')
    grp = parser.add_mutually_exclusive_group()
    grp.add_argument('-n', '--notransfer', action='store_true',
        help='Do not add a transfer address')
    grp.add_argument('-t', '--transfer', type=check_xfer, default=-1,
        help='Specify transfer address instead of using DDOS entry')

    args = parser.parse_args()

    with open(args.ddosfilename, 'rb') as f:
        magic55, type, start, size, dosentry, magicaa = \
        struct.unpack('> 2B 3H B', f.read(9))

        if magic55 != 0x55 or type != 2 or magicaa != 0xaa:
            print('{} is not a DDOS binary'.format(args.ddosfilename))
            exit(1)

        data = f.read(size)

        if len(data) != size:
            raise Exception('Incorrect size field in binary header')

        with open(args.flexfilename, 'wb') as f:

            while (len(data) > 0):
                packet = data[:248]
                data = data[248:]
                packlen = len(packet)
                f.write(struct.pack('> B H B', 2, start, packlen))
                f.write(packet)
                start += packlen

            if not args.notransfer:
                if args.transfer < 0:
                    xfer_addr = dosentry
                else:
                    xfer_addr = args.transfer

                if (size > 0) or (args.transfer >=0):
                    f.write(struct.pack('> B H', 0x16, xfer_addr))
